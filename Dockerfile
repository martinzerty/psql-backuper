FROM python:3.10

RUN mkdir -p /var/psql_backuper/folder
RUN mkdir -p /var/psql_backuper/folder/data
RUN mkdir -p /var/psql_backuper/folder/logs

COPY backuper.py /var/psql_backuper/backuper.py
COPY compressor.py /var/psql_backuper/compressor.py
COPY encrypter.py /var/psql_backuper/encrypter.py
COPY main.py /var/psql_backuper/main.py
COPY requirements.txt /var/psql_backuper/requirements.txt
COPY cron_launch /var/spool/cron/crontabs/root

RUN chmod +x /var/psql_backuper/backuper.py
RUN pip3.10 install -r /var/psql_backuper/requirements.txt

RUN apt-get update
RUN apt-get install postgresql-client
RUN apt-get install -y cron

CMD cron -l 2 -f

# https://github.com/Ekito/docker-cron/blob/master/Dockerfile