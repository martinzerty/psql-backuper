import dotenv
import pb.encrypter as encrypter
import pb.compressor as compressor
import pb.backup as backup

if __name__ == "__main__":
    dotenv.load_dotenv()
    backup.backup_db('folder/data')

    enc_name = encrypter.encrypt_to_new_dir('folder')
    compressor.zip_folderPyzipper(enc_name, f'{enc_name}.zip')

    dir = compressor.decompress(f'{enc_name}.zip')
    encrypter.decrypt_dir(dir)
    
    try:
        backup.upload_content(f'{enc_name}.zip')
    except Exception as e:
        print(str(e))