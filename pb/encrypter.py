from cryptography.fernet import Fernet
import os
import shutil

def encrypt_file(path: str) -> None:
    f = Fernet(os.getenv('ENC_KEY'))
    with open(path) as reader:
        content = reader.read()
    
    with open(path, 'w') as writer:
        if len(content) > 0:
            writer.write(f.encrypt(content.encode('utf-8')).decode('utf-8'))

def decrypt_file(path: str) -> None:
    f = Fernet(os.getenv('ENC_KEY'))
    with open(path) as reader:
        content = reader.read()
    
    # print("content : ", content.encode('utf-8'))
    with open(path, 'w') as writer:
        writer.write(f.decrypt(content.encode('utf-8')).decode('utf-8'))

def copytree(src, dst, symlinks=False, ignore=None):
    for item in os.listdir(src):
        if not item.split('/')[-1].startswith('.'):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                print(s, " is dir")
                shutil.copytree(s, d, symlinks, ignore)
            else:
                if len(s.split('.')) > 1:
                    shutil.copy2(s, d)

def remove_dir_if_exists(dir):
    if os.path.exists(dir):
        shutil.rmtree(dir)

def encrypt_to_new_dir(dir: str) -> str:
    """dir has to be the absolute path. All files must have an extension and no hidden file (starting with '.')"""
    encrypted_dir_name = dir.replace(dir.split('/')[-1], dir.split('/')[-1] + "_enc")
    remove_dir_if_exists(encrypted_dir_name)
    copytree(dir, encrypted_dir_name)
    for path, subdirs, files in os.walk(encrypted_dir_name):
        for name in files:
            path_to_encrypt = os.path.join(path, name)
            print(path_to_encrypt )
            if not path.split('/')[-1].startswith('.'):
                encrypt_file(path_to_encrypt)
    return encrypted_dir_name

def decrypt_dir(dir: str) -> None:
    for path, subdirs, files in os.walk(dir):
        for name in files:
            try:
                path_to_encrypt = os.path.join(path, name)
                decrypt_file(path_to_encrypt)
            except Exception as ex:
                print(f"Can not decrypt : {name}:\n{ex}")