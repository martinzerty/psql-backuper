import subprocess
import os
import boto3
from botocore.exceptions import ClientError

def backup_db(path):
    with open(f"{path}/{os.getenv('DB_URL').split('/')[-1]}.sql", 'w') as backup:
        subprocess.run(['pg_dump', os.getenv('DB_URL'), '--column-inserts', '-c'], stdout=backup)

def upload_content(file_name):
    s3 = boto3.client(service_name='s3',
                    aws_access_key_id=os.getenv('S3_ACCESS_KEY'),
                    aws_secret_access_key=os.getenv('S3_SK'),
                    endpoint_url=os.getenv('S3_URL'),)
    
    try:
        r = s3.upload_file(file_name, os.getenv('S3_BACKUP_BUCKET'))
        return True
    except ClientError as e:
        print(str(e))
        return False